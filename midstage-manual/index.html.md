# Welcome to the $COMPANY Handbook

## Introduction

This handbook describes how we do things at $COMPANY. It is based on the [Midstage manual](http://manual.midstage.org), an open source handbook with best practices that midstage startups like $COMPANY can [[adapt and adopt]].

* [Why this handbook](about/why_this_handbook.md)
* [How to use this handbook](about/how_to_use_handbook.md)

It is important to realize the handbook always reflects the current state of learning in $COMPANY.

Below is a quick overview of all the documentation we are collecting in this handbook. It contains five major sections:

1. Directions and Priorities
2. Functional Areas
3. Cross-functional Processes 
4. Projects and Initiatives
5. Working at $Company

## Directions and Priorities

Overview of our $Company Plan-at-a-Glance:

| Horizon | Inspiration | Goals | Market Presence | Success Metrics |
| :-- | :-- | :-- | :-- | :-- |
| **[10+y: Core Identity](directions/core/index.html.md)** | [Purpose](directions/core/purpose.html.md) | [Values](directions/core/values.html.md) | [Growth levers](directions/core/growth_levers.html.md) | [BHAG/10y target](directions/core/bhag.html.md) |
| **[3-5y: Strategy](directions/strategy/index.html.md)** | [3-5y Vision](directions/strategy/mission.html.md) | [Strategic Thrusts](directions/strategy/thrusts.html.md) | [3-5y Brand/Promise(s)](directions/strategy/differentiation.html.md) | [3-5y KPIs](directions/strategy/metrics.html.md) |
| **[1y: Mid-term Plan](directions/annual/index.html.md)** | [1y Breakthrough](directions/annual/breakthrough.html.md) | [Annual OKRs](directions/annual/OKRs.html.md) | [Annual Launch Roadmap](directions/annual/launch.html.md) | [1y KPIs](directions/annual/metrics.html.md) |
| **[1q: Short-term Plan](directions/quarterly/index.html.md)** | [Quarterly Theme](directions/quarterly/theme.html.md) | [Quarterly OKRs](directions/quarterly/OKRs.html.md) | [Quarterly Release Roadmap](directions/quarterly/release.html.md) | [1q KPIs](directions/quarterly/metrics.html.md) |
| **[Every Sprint](directions/sprints/index.html.md)** | [Mini-games](directions/sprints/mini-games.html.md) | [Commits](directions/sprints/commits.html.md) | [Feature(s)/Fix(es)](directions/sprints/featuresfixes.html.md) | [Process Outputs](directions/sprints/metrics.html.md) |
| | | | | |
| _**[Inputs](directions/inputs/index.html.md)**_ | _[PESTLE trends](directions/inputs/trends.html.md)_ | _[Key Employee Inputs](directions/inputs/employees.html.md)_ | _[Key Customer Inputs](directions/inputs/customers.html.md)_ | _[Key Results Analysis](directions/inputs/results.html.md)_ |



## Functional Areas

Innovation Engine | Acquisition Engine | Satisfaction Engine | Enablement Engine
:--- | :--- | :--- | :---
Keeps $Company at the forefront of the market with a competitive offering | Brings in a maximum number of new customers | Ensures our clients are happy and ready to buy more | Keeps our employees successful and on the right path
[Product](functions/product.md) | [[Marketing]] | [[Customer Success]] | [[People]] 
[[Engineering]] | [[Sales]] | [[System Operations]] | [[Finance]]

## Cross-functional Processes 

### [Company Metrics](/metrics)

* [OKRs vs KPIs](/processes/metrics/)
* [(Key) Performance Indicators](/metrics/performance_indicators.html)
* [Objectives and Key Results](/metrics/okrs.html)
* [Breakdown and Cascading](/metrics/cascading.html)

### [Company Rhythms](/rhythms)
 
* [Quarterly Rhythm](/processes/rhythms/quarterly.html) - [Quarterly Business Review](/processes/rhythms/quarterly_business_review.html)
* [Annual Rhythm](/rhythms/annual.html)

## Projects and Initiatives

## Working at $Company

* Onboarding
* Performing
* Working in Teams
* Progressing and getting promoted
* Leading/Managing a Team
* Off-boarding and Alumni

[[Lorem]]
