{::comment}This is default content from the Midstage Manual. Please replace this with your own startup's content or get help from ask@midstage.org to draw it up{::/}

# $Company Core Purpose

Our Core Purpose is "Making the World a Better Place"

## Why this Core Purpose

* We live in a world with many bad initiatives and with many people only looking out for themselves;
* After having worked at $Company we want to be proud of what we accomplished, of what we did well;
* Accomplishments like money, fame, power and attractiveness can feel important short-term, but often make us feel jaded over time;
* Accomplishments like truly making a difference in the world, building concrete improvements stay with us for a lifetime.

## Why a Core Purpose

Every company needs a "reason for being", an vision that inspires employees, customers and invesrtors to want to help the company do better. 

Simon Sinek calls this "Start with Why". Why does what we do matter, why does the world need us, why do we care that $Company exists?

The purpose or the "why" helps everyone align on an emotional level, beyond the sheer numbers and beyond the daily grind. It can help us hold ourselves accountable to "being on the right path", to ask ourselves "is this really still fulfilling our purpose?"

## When to use the Core Purpose

1. When choosing between different courses of action, which fulfills our core purpose better?
2. When deciding between focusing and expanding, which would help us stay "truer" to our core purpose?
3. When we feel like we're not making much progress, can we reinspire ourselves and our team by pointing to the difference we're making in the world?
4. When we are overly focused on routine, can we paint the "bigger picture" using our Core Purpose to ask people for new ideas?


