# The Quarterly Business Review

Each quarter, the Executive Team meets to review the ongoing and upcoming quarters.

## Why a QBR

The QBR is the moment for our Executives to step back from the day-to-day pressures, and make time to answer some fundamental questions about the business:

1. How are we doing in reaching [[our vision]]?
2. Which are the biggest road blocks in the way of our vision?
3. Which road block(s) should we focus on removing first?
4. What is each of us going to focus on in the next quarter?

The key context here is for executives to take time to identify [[the important over the urgent]]. Without a QBR, or without following such an agenda, executives tend to focus too much on urgent issues coming at them, and lose sight of the important milestones that really help us reach our vision.

## What does the Executive Team discuss during a QBR?

A typical QBR agenda looks as follows:

1. Review performance against plan, specifically [[Quarterly OKR]] progress against the [[One-Page Plan]];
2. Review patterns in input such as customer feedback, employee feedback, investor feedback etc;
3. Reaffirm the longer-term vision from the [[One-Page Plan]], especially [[Annual OKRs]] and [[3-year OKRs]];
4. Identify the key roadblocks impeding us from reaching our vision. This section may include a deep-dive learning into a particular problem that the Exeuctive Team feels $Company has made too little progress on;
5. Prioritize and select as few as possible road blocks to reduce or remove during the coming quarter;
6. (Re)affirm ownership, mission and success metrics for each key department and each key initiative.

## How does the Quarterly Business Review set priorities?

As the team discusses these topics one by one, it is important to keep a few principles in mind:

1. The QBR is primarily about each executive learning about each of the others' perspectives. 
    - We optimize for gathering perspectives before discussing perspectives. For example, we will often ask all executives to write down 3 possible answers to a question on sticky notes, and group them, before we start discussing as a group.
    - We try to avoid the approach "let an executive give a presentation and then the team asks questions", especially when thinking through new problems and setting new priorities.
2. The leader should learn as much (or more) from the participants as the participants than the participants from the leader
    - We typically ask the Head of the Company to speak their mind last, after we have heard from (all) other participants.
    - Every participant should take a "whole company" perspective, as Head of Company rather than Head of their specific function.
3. Trusted, safe environment.
    - Every participant should feel safe to build on and/or challenge another participant respectfully, in the interest of ever better thinking for the company.
    - We make it clear when is the time to debate things, and when is the time to commit to a decision.


## When does the Quarterly Business Review take place?

Most companies set a fixed week every quarter where the Quarterly Business Review takes place, so that

- it can be scheduled well in advance
- outside resources such as meeting room, hotels and facilitators are avaiable.

{::comment}Update the next line for your specific company or comment it out if unsure{:/}

At $Company, we have our Quarterly Business Review in the Xth week of every quarter.

## Who participates in the QBR

{::comment}Comment out the following lines if not sure yet{:/comment}

### By Function

<!--
At $Company, we typically have the following executives participate in the QBR:
- Name/Function
- Name/Function
- Name/Function
- Name/Function
- Name/Function
-->

Participants to the Quarterly Business Review are the CEO and all executives who report directly to the CEO. In companies where two top executiveas are co-leading the companuy (e.g. CEO and CTO, or CEO and President), all executives reporting directly to either of those two should participate. Typical participating functions include:

- Head of Company
- Head of Customer Success
- Head of Finance
- Head of Marketing
- Head of People
- Head of Product
- Head of Sales
- Head of Services
- Head of Technology

### Targeting between six and twelve participants

Ideally, the Quarterly Business Review has between six and twelve participants. This ensures that everyone experience a good balance between listening to others and contributing themselves.

In companies where the total number of direct reports to top executives exceeds twelve, executives with a key [[line responsibility]] should be invited before executives with a [[staff responsibility]]. 

### Using outside facilitator

At $Company, the QBR is typically organized by our Midstage Facilitator{::comment} Name and link{:/comment}, who also serves as coach to our CEO. This is to ensure sufficient outside perspective and avoid "stewing in our own juices"

## Where does the Quarterly Business Review typically take place?

The QBR typically takes place in a location outside of the office. This is to ensure that 

1. our executives can take "proper distance" from the day-to-day work. 
2. all executives are physically present in the same room

When meeting in one place is not possible (due to time constraints or e.g. during the 2020 pandemic), the QBR takes place over video conference, in the form of several remote sessions. These are most effective when everybody dials in from a separate location, i.e. no hybrid meetings with a central meeting room and some remote participants.
