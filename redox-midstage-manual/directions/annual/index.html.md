# $Company Operational Plan 2020

This section focuses on the results we are looking to achieve this fiscal year, from January through December 2020.

1. [Annual Critical Number](annual/critical_number.md): what is the most important metric for us to "fix" this year?
2. [Annual Initiatives/OKRs](annual/initatives_OKRs.md): what company-wide initiatives are we looking to achieve this year?
3. [Annual KPI targets](annual/KPIs.md): what key metrics will drive our success this year, and what targets have we set for them?
