# Plan for Every Sprint

A "sprint" is $Company's shortest possible time period of work. 

It reflects the period for which we set a short-term goal, then how much time we give to reach that short-term goal, then after how much time we check back in if the goal has been achieved.

## How long is a sprint? At $Company, it is two weeks

In many traditional companies, the default length of a sprint is one week. This means team meetings every week, executive meetings every week, reports every week etc.

In many tech companies, however, the product and engineering departments found that checking in every week generates too much overhead to get things done. This is why many proponents of the agile philosophy developed sprints of two or three weeks, someties eve longer.

At $Company we have settled on sprints of two weeks, for all departments (from technical to sales and from finance to executives).

