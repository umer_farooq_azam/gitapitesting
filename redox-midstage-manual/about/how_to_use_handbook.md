## How to use this handbook

This handbook is meant to provide everyone at $COMPANY with the latest, agreed, clear guidelines on how we do things. 

1. If you don't know how to do things, search this handbook before asking someone.
2. If you get a question from someone on how to do things and the answer is in this handbook, please *kindly* refer them to the right page.
3. If you get a question from someone on how to do things and the answer is anywhere else but in this handbook, then point the person to the answer *and* hold them accountable to document the answer directly in this handbook. 
4. If you are responsible for a decision that changes a practice documented in this handbook, it is your responsibility to ensure that the new practice gets updated in this handbook. Decisions documented in a presentation deck, a Google doc, a spreadsheet, a recording, in meeting minutes don't count. Unless they are documented in this handbook. 

Please don't worry, no-one at $COMPANY knows (or is supposed to know) all this content by heart. In fact, knowing content by heart would go against the philosophy of constant updates and improvements. Even for processes and practices you are intimately familiar with, please keep an eye on ongoing changes and improvements.
