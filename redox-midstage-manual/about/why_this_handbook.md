# Why this $Company Handbook

As companies grow, the time spend on finding and correcting the right knowledge goes up exponentially. At $COMPANY, we want to be efficient in delivering value for our customers. To do so, we need to find the right answers fast and avoid drowning in thousands of Google docs, emails, meetings etc. Especially when many/most/all employees are working remotely.

The principle of [Handbook First Documentation](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/), initially developed at Gitlab, is our answer to this problem.

1. We make sure we document **everything we do in one place: this handbook.** Every other document you use (presentations, Google docs, spreadsheets, recordings, meetings, Slack discussions etc) are considered fleeting and should only be used to prepare the decision in the form of a change to this handbook.
2. This handbook contains **only our current practice.** Not past versions, not different opinions, not arguments why things changed, not open comments without answers. Only currently official versions.
3. On the other hand, anything in **this handbook is always in draft,** and therefore open to constant improvement. We encourage everyone at $COMPANY to propose iterative improvements wherever they see an opportunity. 
4. This handbook represents **our common effort**. While individual $COMPANY people may "own" content on a given page, we encourage all "owners" to keep accepting contributed iterations to make their practice/process/content ever better and feel like a common effort.
