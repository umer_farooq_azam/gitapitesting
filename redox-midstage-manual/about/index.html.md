---
title: About the Midstage Manual
---

# Midstage Manual

This site provides an open alpha version of an open source manual for midstage tech startups.

## What each employee needs to know

A good startup's manual covers:

1. why the startup is in business, 
2. what it tries to accomplish, 
3. how it goes about doing things,
4. who is responsible for what,
5. and what success looks like.

In this sense, the startup manual goes well beyond the typical employee manual.

## Easily Adoptable

The Midstage Manual collects best practices for mid-stage tech startups in a format that can be deployed as an in-house employee handbook.

## Easily Adaptable

* All content is hosted on a git repository that midstage startups can easily fork to make their own.
* As with any git fork, each startup can make its own version of the manual while still adopting new suggested pages as they come in from the central repository.
* Often-used company termn such as $COMPANY, $CEO etc are easily replaced globally with the real names, titles etc.

## Alpha version

This site was launched in July 2020 to start hosting the first content relevant to midstage startups currently working with Scaleup Allies.
