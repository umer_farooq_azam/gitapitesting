# $Company Product Organization

The Product organization is in charge of designing and delivering to our target markets the most compelling product experience possible for the money our customers are willing to pay.

## Product in the broadest sense

We define "product" in the broadest sense of the entire customer experience, across all functions and along the entire lifecycle.

## Coordinating role

This means that the Product organization's core role is one of coordination and alignment of other functions, where much of the delivery of day-to-day work happens.

