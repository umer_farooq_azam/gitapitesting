
# Metrics: OKRs and KPIs


- [About KPIs](performance_indicators.html)
- [About OKRs](okrs.html)

## About setting targets

At $COMPANY we believe in explicit performance metrics.

This is to avoid misunderstandings about people or teams thinking they do a good job based on one criterion, while their boss thinks they are doing a bad job based on another criterion.

Companies or teams that don't set explicit performance metrics will see team members each inventing their own (often subconscious) performance standards. For example:

- At another scaleup company, a product manager believed they did a great job if they answered every incoming email within five minutes. But their boss was looking primarily for the product getting more users.
- At yet another mid stage company, a customer success manager believed they had excellent performance because they helped one technically averse customer finally onboard their team. But their boss felt they should have focused on finding more upselling opportunities with the bigger customers.

At $COMPANY we want to have explicit agreement what matters most, both in our day-to-day work and in projects we run to make the company better.

## Two types of performance metrics: KPIs and OKRs

At $COMPANY we consider [(Key) Performance Indicators](performance_indicators.html) the ongoing metrics by which the company, each output area and each team measures its performance.

When we take an initiative or launch a project to substantially move the needle on one of our KPIs, the project has an [Objective and Key Results](/okrs.html) (OKR).

![KPIs and OKRs differ by imopact](images/kpis_and_okrs_impact.png "KPIs and OKRs differ by imopact")

## Why we need both KPIs and OKRs

Some companies, especially smaller startups, use only OKRs to measure their performance. This works best when most of the important work the company does is in projects, and there is little "day-to-day work" to measure apart from the projects.

Some other companies may use only KPIs, or measure the stable indicators across the business. This works best when the business is mature and most business results derive from the steady flow of day-to-day work. In those businesses, projects do not matter as much as in high-growth, midstage startups.

Some other companies may use only OKRs, but make no clear differentiation between day-to-day results and "moving the needle" projects. This can lead to a confusion about accountabilities, and to people feeling overwhelmed between too many priorities.

At $COMPANY we have found it most effective to make a clear differentiation between (1) the ongoing expected results of our day-to-day work in KPIs and (2) the explicit projects that we prioritize to really move the needle on a specific KPI, tracked as an OKR.

## What gets measured, gets managed -- so measure what is important. Not what is convenient.

There's a phrase "What gets measured gets managed" - and while that's true, one thing leaders struggle with is that their people are managing to a whole lot of internal things (hours spent by our teams, dollars spent by our teams, etc.) because that's what they can measure. The trick is not to just stick with the metrics you have, but to pick your metrics from a goal-focused mindset. 

Indeed “what gets measured gets managed” often bastardizes into “we manage it because we can measure it”. Of course the caveat is that, by definition, something that is easily measured is almost never something that **differentiates* you. So the challenge then becomes to first define what is most important (objectives) and then to somehow make that measurable with a single objective and accepted indicator (key result).

Technically the challenge will be to set up some new instrumentation; managerially the bigger challenge will typically be to select one indicator out of many different measurable possibilities and have that accepted as the key indicator for the overall objective. Eg. in many SAAS companies it’s “median time between signup and AHA moment” as an indicator of “reduced friction”.
