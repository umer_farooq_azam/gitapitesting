---
title: OKR Annual Rhythm
---


# OKR Annual Rhythm

- TOC
{:toc}


## Overview

| Monday | CEO/Exec Team | Output Areas | Departments | Frontl.Teams |
| :----- | :----- | :----- | :----- | :----- |
| Q4-11 | Review 3y KPIs |  |  |  |
| Q4-10 | Prep Annual KPIs |  |  |  |
| Q4-9 |  |  |  |  |
| Q4-8 | See [quarterly rhythm](quarterly.html) |  |  |  |

## Extra cascading for annual OKRs

In the third quarter of each year, $COMPANY reviews and resets targets for
- the next three years;
- the next year.

This means that we identify, update and/or refresh our 3 year programs and our annual OKRs. 

It also means that we add an extra "annual goals" deliverable to the normal OKR quarterly rhythm

## Annual OKR Rhythm—Additions to Quarterly Rhythm 

### 11 Mondays before the start of Q4 ### 

1. The $EXECUTIVE_TEAM reviews progress versus 3-year KPIs and OKRs
2. The $COS launches annual data gathering efforts such as team performance analysis

### 10 Mondays before the start of Q4 ### 

The $COS initiates the Annual OKR Process:

1. By creating a "blank sheet" for next year's OKRs
2. By asking $CEO and $CFO to confirm the company's [3] most important numerical targets for the next year. 

### 9 Mondays before the start of Q4 ###

$CEO sends out:

- the top [3] company goals for the next year
- to his/her direct reports
- in the form of company KPI targets
- that match roughly with the company's key "output areas".

The $EXECUTIVE_TEAM reviews progress versus annual KPIs and OKRs

### 8 Mondays before the start of Q4 ###

... follows the rest of the quarterly OKR rhythm, but asks teams to submit both annual (for next year) and quarterly (for Q4 of this year) OKRs



