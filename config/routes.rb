require 'gollum/app'
class App < Precious::App
  before { assign_author }

  helpers do
    def assign_author
      session['gollum.author'].symbolize_keys!
    end
  end
end

Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks', sessions: 'users/sessions' }

  authenticated :user do
    wiki_options = {
      h1_title: true,
      user_icons: 'gravatar',
      live_preview: true,
      allow_uploads: true,
      per_page_uploads: true,
      allow_editing: true,
      index_page: "index.html",
      css: true,
      js: false,
      mathjax: true,
      emoji: true,
      show_all: false,
      template_dir: 'lib/gollum',
      company_name: ENV['COMPANY_NAME']
    }

    Precious::App.set(:gollum_path, Rails.root.join(ENV['SUBMODULE_PATH']).to_s)
    Precious::App.set(:default_markup, :markdown)
    Precious::App.set(:wiki_options, wiki_options)

    mount App, at: '/'
  end

  root to: "pages#home"
end
