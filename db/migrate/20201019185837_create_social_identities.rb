class CreateSocialIdentities < ActiveRecord::Migration[6.0]
  def change
    create_table :social_identities do |t|
      t.string :provider
      t.string :uid
      t.references :user

      t.index [:provider, :uid], name: 'index_social_identities_on_provider_and_uid'
      t.index :uid

      t.timestamps
    end
  end
end
