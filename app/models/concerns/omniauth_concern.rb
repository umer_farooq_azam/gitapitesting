# frozen_string_literal: true

module OmniauthConcern
  extend ActiveSupport::Concern

  included do
    devise :omniauthable, omniauth_providers: %i[google_oauth2]

    def self.from_omniauth(auth)
      social_identity = SocialIdentity.find_by(provider: auth.provider, uid: auth.uid)
      return social_identity.user if social_identity

      email = auth['info']['email']
      existing_user = find_for_database_authentication(email: email.downcase)

      if existing_user
        existing_user.add_social_identity(auth).save

        return existing_user
      end

      create_new_user_from_oauth(auth, email)
    end

    def self.new_with_session(params, session)
      super.tap do |user|
        data = session['devise.oauth.data']

        if data.present?
          user.email = data['info']['email'] if user.email.blank?
          user.add_social_identity(data)
        end
      end
    end

    private

    def self.create_new_user_from_oauth(auth, email)
      user = User.new({ email: email, password: Devise.friendly_token[0, 20], name: auth['info']['name'] })
      user.add_social_identity(auth).save
      user
    end
  end

  def add_social_identity(data)
    social_identities.build({ provider: data['provider'], uid: data['uid'] })
  end
end
