# frozen_string_literal: true

class User < ApplicationRecord
  include OmniauthConcern

  has_person_name

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

  has_many :social_identities, dependent: :destroy

  validates :email, presence: true, uniqueness: true
  validates :first_name, :last_name, presence: true
end
