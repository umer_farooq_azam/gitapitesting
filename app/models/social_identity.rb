# frozen_string_literal: true

class SocialIdentity < ApplicationRecord
  belongs_to :user

  validates :uid, uniqueness: { scope: %i[provider] }
end
