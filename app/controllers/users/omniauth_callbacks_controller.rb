# frozen_string_literal: true

module Users
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def google_oauth2
      @user = User.from_omniauth(auth_data)

      if @user.persisted?
        session['gollum.author'] = { name: @user.name, email: @user.email }
        flash[:notice] = t 'devise.omniauth_callbacks.success', kind: 'Google'

        sign_in_and_redirect @user, event: :authentication
      else
        session['devise.oauth.data'] = auth_data.except('extra')

        redirect_to new_user_registration_url, alert: @user.errors.full_messages.join("\n")
      end
    end

    protected

    def auth_data
      request.env['omniauth.auth']
    end

    def after_omniauth_failure_path_for(_)
      root_path
    end
  end
end
