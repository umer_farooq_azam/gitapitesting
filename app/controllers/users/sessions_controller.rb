# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    def destroy
      super { session['gollum.author'] = nil }
    end
  end
end
