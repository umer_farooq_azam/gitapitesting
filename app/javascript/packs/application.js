require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require('jquery')

import jquery from 'jquery';
window.$ = window.jquery = jquery;

import 'bootstrap';
import "@fortawesome/fontawesome-free/js/all";
import 'custom/script';
import 'controllers'

import '../stylesheets/application'
