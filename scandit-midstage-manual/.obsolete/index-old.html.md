---
title: Welcome to the $COMPANY Handbook
---


This handbook describes how we do things at $COMPANY, in line with [Handbook First Thinking](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/). 

It is based on the [Midstage manual](http://manual.midstage.org), an open source site where every midstage startup can collect sample best practices to adapt and adopt.

## Table of Contents

### [Company Rhythms](/rhythms)
 
* [Quarterly Rhythm](/rhythms/quarterly.html)
* [Annual Rhythm](/rhythms/annual.html)

### [Company Metrics](/metrics)

* [OKRs vs KPIs](/metrics/)
* [(Key) Performance Indicators](/metrics/performance_indicators.html)
* [Objectives and Key Results](/metrics/okrs.html)
* [Breakdown and Cascading](/metrics/cascading.html)

