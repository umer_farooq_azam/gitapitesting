# $COMPANY Strategic Plan 

This section explains 3-year directions and decisions that will help us get closer to our long-term vision, such as:

1. [3-year Mission](strategy/mission.md): what is the "next mountain to climb" for $COMPANY on the way to our [BHAG](/core/bhag.md)?
2. [Ideal Customer](strategy/customer.md): which persona is the ideal buyer/user for $COMPANY's products? 
3. [Value Proposition](strategy/promise.md): what are the key benefit(s) we are differentiating with from competitors?
4. [Sandbox Expansion](strategy/sandbox.md): in what segments and geographies do we offer what products by when?
6. [3-year strategic thrusts/OKRs](strategy/thrusts_OKRs.md): what company-wide strategic thrusts are we looking to achieve within three years?
5. [3-year KPI targets](strategy/KPIs.md): what key metrics will drive our success and what targets have we set for them?
