# $Company Core Values

{::comment}These are default values from the Midstage Manual. Please replace with your own core values once set.{:/comment}

## Core Value 1: Hungry

We like to work with people who are hungry to make an impact. [more about value #1](value1.html.md)

## Core Value 2: Humble

We like to work with people who appreciate everybody's contributions, not just their own. [more about value #2](value2.html.md)

## Core Value 3: Happy

We like to work with people who are positive about life and inspire us all with their energy. [more about value #3](value3.html.md)

## Why do we need Core Values?

Core values help everyone at a company take the right decisions when we don't have prescribed steps of operating (yet).

They are meant as a general guideline for any situation you could encounter, and allow you to make the right decision by yourself.

Core values are especially important at a midstage startup like $Company because:

1. The majority of our people has joined the company less than 2-3 years ago, so many people need guidance from a few.
2. We are still figuring out and optimizing much of our work, which means it is not fully documented and prescribed yet.

## What should you do with Core Values?

1. Whenever you prepare or take a decision, try to take guidance from the core values. 
2. Whenever you see evaluate someone else's decision at the company, try to evaluate them not against what you think is right or wrong, but on whether they "live the core values"
3. When you are not sure a proposed decision is in line with our core values, ask the Directly Responsible Individual if they can explain better how they are aligned. This is especally important coming **from newer employees,** as they have a fresh view on incosistencies that may have slipped in over time.


