# Adapting and Adopting the Midstage Manual

The [[Midstage Manual]] is written as a quick starting point for midstage startups to fix their internal knowledge sharing problems without having to start from scratch.

## Quick adoption

The Midstage Manual is an open source git repository. This means that it is easy to clone and start making changes, just like a startup would install any other software library.

All text in the Midstage Manual is written in Markdown, to maximize compatibality with a large number of wikis, web servers and internal knowledge systems.

## Easy adaptation