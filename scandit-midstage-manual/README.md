# Midstage manual README

This repository provides a template Employee Manual for midstage (technology) startups.

## A midstage manual

One growing pain that startups experience when entering the mid-stage is a lack of clarity.
The founding team and early employees have the base of internal knowledge mostly in their heads. 
The only way for new employees to learn is to ask longer-tenured employees lots of questions.
Not only does this not scale.
It also makes it challenging to update the company's knowledge and drive changes. 
Especially when you want your employees to feel empowered to contribute. And even more so, when everybody is working remote.

## Handbook-first philosophy

The alternative, as pioneered by [Gitlab Inc](https://about.gitlab.com/), is working [Handbook-First](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/).
It acknowledges that building and scaling a startup company implies building and scaling the company's knowledge.
It assigns the Company Handbook the role of carrying all the company's current knowledge.
This means any change or innovation in the company's practices or knowledge can be defined as an edit in the Company Handbook. 
No more sitting in Zoom meetings, delving through PowerPoint decks or making sense of Google docs. 
Either the guidance is clearly defined in the latest version of the Company Handbook, or it didn't happen.

## Solving the blank sheet problem

Popular "knowledge base" providers such as Gitlab, [Atlassian](https://atlassian.com) and [Notion](https://notion.app) have made available great templates for startups to write up their employee guidelines, document a strategic plan and generally provide a great technical environment for building a knowledge base. 

Technology is not the problem. Content generally is. 

Most midstage startups start realizing they need a knowledge base/wiki. But after they have bought the software, they don't know where to start. Even well-meaning attempts at blank employee manuals don't tell startups what some good default values are, that they can adapt to their needs.

This is really why we have created this midstage manual. To provide a template not only for the technology and the layout, but also for some good default content that startups can adopt-then-adapt right away. 


## Easily adoptable

## Easily adaptable

