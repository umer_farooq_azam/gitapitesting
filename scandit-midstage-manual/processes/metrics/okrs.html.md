---
title: Objectives and Key Results 
---

# OKRs are for significant improvements

- TOC
{:toc}


![alt text](images/kpis_and_okrs_impact.png "KPIs and OKRs differ by imopact") 

When the few percent performance each quarter that you can expect from a [KPI](performance_indicators.html) is not enough, the company or a team needs to put extra effort behind an improvement. 

This is when we launch a project/initiative or OKR. OKR stands for "Objective and Key Results". 

## Objective

The "Objective" is the overall, qualitative, often inspiring, end goal of the project. For example "$COMPANY in Africa".

## Key Results

The Key results are the measures by which we can see if the project was a success by its deadline. For that reason, they are often written in perfect tense:

1. "Closed 3+ deals with customers headquartered in Africa"
2. "Built pipeline of 10+ Sales Accepted Opportunities in Africa"
3. "Gathered leads for clients in 10+ African countries"

## Antipattern: not key results but key sub-projects ###

The Key Results should be pure measurements of the success of the objective. They should not be confused with [breakdowns/subprojects/different streams](cascading.html) supporting the objective. 

E.g. if the KPI is "$10M in sales" and the Objective is "Boost E-commerce sales" then:

1. **Good Key Results** are "sold $1M via E-commerce channel", "Won 1000 new paying customers in E-Commerce" and "Maintained profit margin in E-commerce at 30%".
2. **Bad Key Results** would be "Integrated Shopify for E-Commerce", "Hired an E-commerce manager" and "Updated Google AdWords campaign for E-Commerce ads". This breakdown should occur not in the key results, but in the cascading of the OKR downwards in the organization.

