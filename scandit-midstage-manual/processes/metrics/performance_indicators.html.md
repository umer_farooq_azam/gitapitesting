# (K)PIs are stable indicators

- TOC
{:toc}


(K)PIs, by nature, are stable metrics. They reflect all the things $COMPANY or a team has learned to measure since when they started to operate.

For example, a sales team may first only measure Annual Contract Value sold. Then they find they have too many deals fall through, and they start measuring close rate. Then they see many opportunities without traction, and they start measuring average-time-to-close. And so on.

When the list of metrics gets too long, the company and individual teams need to agree which metrics are most important. This prioritization yields the "key" performance indicators: the 2 to 4 metrics that are most important and that everybody should pay attention to improving.

Every team is expected to ensure no Key Performance Indicator gets worse, and to gradually improve at least one of them with a few percent each quarter.

![alt text](images/kpis_and_okrs_impact.png "KPIs and OKRs differ by imopact") 

When a few percent each quarter is not enough, the company or a team needs to put extra effort behind an improvement. This is when we launch a project/initiative with an [Objective and Key Result](okrs.html).