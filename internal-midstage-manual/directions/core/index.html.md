# About $COMPANY Core Decisions

Core decisions are the the kind of decisions that we believe in so deeply, that we can expect them to be stable long-term. 

Even if $COMPANY undergoes major changes and/or our markets change radically, our core identity decisions are what we can all hold onto to define a new mission and to respond to the environment with our inner strength intact.

This section explains $COMPANY's "core identity" decisions such as:

1. [Core Values](values.html.md): our beliefs and the kind of people we like to work with
2. [Core Purpose](purpose.html.md): what is our reason for being, how do we make the world a better place and why does this matter?
3. [Core Competences](competences.html.md): what are the (few) capabilities we focus on being world class at?
4. [Core Success Metric](succes_metric.html.md): what does success look like and how can we measure this objectively?
5. [BHAG](bhag.html.md): what is our "big hairy audacious goal" that we are looking to get to, long-term?
