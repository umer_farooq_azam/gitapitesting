---
title: OKR Quarterly Rhythm
---

# OKR Quarterly Rhythm 

- TOC
{:toc}

## Overview

| Monday | CEO/Execs | Output Areas | Departments | Frontl.teams |
| :----- | :----- | :----- | :----- | :----- |
| Q-9 | Review Progress |  |  |  |
| Q-8 | Launch OKR process |  |  |  |
| Q-7 | Top 3 company KPIs |  |  |  |
| Q-6 |  | "how to get there" → Draft company OKRs |  |  |
| Q-5 | QBR: finalize comp.KPIs/OKRs |  |  |  |
| Q-4 | All-mgr meeting | Finalize cross-functional KPIs and OKRs | Finalize functional KPIs and OKRs |  |
| Q-3 | All-hands mtg |  |  | Finalize team KPIs and OKRs |
| Q-2 |  | Confirm OKR teams | Confirm OKR teams |  |
| Q-1 | Final "how to get there" presentations |  |  |  |
| Q+1 | Start tracking | Start tracking | Start tracking | Start tracking |


## OKR Process Initiation ##

### Q-9: 9 Mondays before the start of the next quarter ### 

1. The Executive Team reviews progress versus annual KPIs and OKRs. 
2. The Chief of Staff launches quarterly data gathering efforts such as employee surveys.

### Q-8: 8 Mondays before the start of the next quarter ### 

The Chief of Staff initiates the OKR Process:

1. By creating a "blank sheet" for next quarter's OKRs
2. By asking CEO and CFO to confirm the company's [3] most important numerical targets for next quarter.

### Q-7: 7 Mondays before the start of the next quarter ###

CFO sends out:

- the top [3] company goals for the quarter 
- to his/her direct reports
- in the form of company KPI targets
- that match roughly with the company's key "output areas".

Each company KPI should have at least one Output Area that can influence its outcome directly. 

Each Output Area should have at least one company KPI that they can influence the outcome of directly. 

If multiple Output Areas can influence the outcome of a KPI directly, then that is even better. 

### Q-6: 6 Mondays before the start of the next quarter ###

#### Output Area Meetings

Key Company Executives gather per Output Area, with their key direct reports. This meeting usually takes 1.5-2h.

#### Key question to ask: how to fill the gaps ####

Each Output Area answers the following questions, for each company KPI:

1. Do we drive this KPI, significantly influence this KPI or only marginally influence this KPI?
2. For KPIs we are driving or significantly influencing, 
   1. What will be the "going concern" result if we do nothing special, just routine day-to-day work?
   1. What will be the result if we push for improvements within the day-to-day work? Which lower-level KPI in particular could we improve?
   1. If a gap remains with the target, what project(s) or initiative(s) could we propose that would more than close the gap? 
3. For KPIs we are *not* driving or significantly influencing, what project(s) or initiative(s) could we propose to other output areas to close any gap?

#### Expectations: company-wide, long list, not approved yet ####

Output area leaders make clear that:

1. The group should propose projects/initiatives both for their own area and for other areas.
2. The discussion is about idea gathering/drawing up a long list---not about deciding/settling on a short list.
3. No work is yet to be started on new projects/initiatives identified.

As output of the meeting, Each Output Area team produces:

1. For each KPI, their draft "plan how the company can reach" each KPI, based on going concern, operational improvements, and projects/initiatives
2. For each project/initative, a draft company OKR: one objective and up to three key results

## Company OKR Decision ##

### Q-5: 5 Mondays before the start of the next quarter ###

The $EXECUTIVE_TEAM meets in a [[Quarterly Business Review]]

## KPI and OKR Cascading ##

### Q-4: 4 Mondays before the start of the next quarter ###

1. The $CEO announces the draft company KPIs and OKRs in an all-manager meeting. 

2. Executives meet with their Output Area teams to identify cross-departmental and departmental OKRs:
   1. \#1 Priority OKRs should be key sub projects that will lead to the company reaching an overall OKR, each phrased as an objective with measurable key results;  
   1. \#2 Priority OKRs should be projects/initiatives that lead to the department delivering an operational KPI improvement, but with no direct bearing on company OKRs;
   1. \#3 Priority OKRs should be projects/initiatives that improve the department's KPIs, but with no direct bearing on company KPIs or OKRs.

3. Output areas confirm the functional KPIs and OKRs each middle manager accepts responsibility for, leading up to and in the new quarter. Each middle manager confirms their (max 4) accountabilities in order of priority.

### Q-3: 3 Mondays before the start of the next quarter ###

1. The $CEO announces the draft company KPIs and OKRs in an all-hands meeting. 

2. All managers meet with their teams to set one or more team OKRs:
    1. \#1 Priority OKRs should be key sub projects that will lead to the company or department reaching an overall OKR, each phrased as an objective with measurable key results;  
    2. \#2 Priority OKRs should be projects/initiatives that lead to the team delivering an operational KPI improvement as identified in 2b above, but with no direct bearing on company OKRs;
    3. \#3 Priority OKRs should be projects/initiatives that improve the department's KPIs, but with no direct bearing on company KPIs or OKRs.

3. Departments confirm the functional KPIs and OKRs each team manager or individual contributor accepts responsibility for, leading up to and in the new quarter. Each middle manager or individual contributor confirms their (max 3) accountabilities in order of priority.

## Preparing for successful launch ##

### Q-2: 2 Mondays before the start of the next quarter ###

1. Company KPI owners update their "how to achieve it" presentations on the basis of updated company, output area, functional and team KPIs and OKRs

2. OKR owners confirm OKR team participants where necessary

### Q-1: 1 Monday before the start of the next quarter ###

1. In individual meetings, company KPI owners present their "how to achieve it" presentations to $CEO and interested members of the $EXECUTIVE_TEAM

2. $COS and $CFO make any last-minute adjustment to KPIs and OKRs

3. OKR owners start organizing their OKR team meetings, project space, tasks etc.

### The first Monday of the next quarter ###

1. The $CEO launches the new quarter with finalized KPIs and OKRs all-hands meeting. 

2. $EXECUTIVE_TEAM and all other teams use the new quarterly KPI/OKR sheet for tracking progress in their weekly meeting.
