---
title: Company Rhythms
---

Company Rhythms are the synchronisations of recurring company processes in the calendar.

At $COMPANY we follow rhythms primarily for our target-setting process:

1. Key is our [Quarterly OKR Process](quarterly.html)
2. In Q3 every year, we add our [Annual OKR Process](annual.html)