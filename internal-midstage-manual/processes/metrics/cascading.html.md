---
title: Cascading and Breakdown
---

# Cascading and Breakdown

- TOC
{:toc}


Performance Metrics form a similar hierarchical tree as the Organization Chart:

1. Company-level KPIs (such as ARR) break down into team-level KPIs such as:

    a. Pipeline value (for demand generation)
    b. New annual contract value (for sales)
    c. ARR per enterprise customer (for product)

2. When KPIs (such as $ Pipeline value) need significant improvement, their goal achievement breaks down between existing KPIs and new OKRs:

    a. Baseline expected pipeline value (from demand generation)
    b. Operational improvements in demand generation (becomes OKR for lower-level team in demand generation)
    c. Launch new digital marketing capability (becomes top OKR for demand generation)

2. Company-level OKRs (such as "Sales from Africa") often break down in team-level Objectives such as:

    a. Obtain African standards approval (for product)
    b. Start marketing campaign in Africa (for demand generation)    
    c. Sell first 3 deals in Africa (for sales)


![alt text](images/kpis_and_okrs.png "Cascading relationships between OKRs and KPIs") 